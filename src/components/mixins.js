exports.mixin = {
    computed: {
        reverseSecond() {
            return this.inputText
                .split("")
                .reverse()
                .join("");
        },
        lengthSecond() {
            return `${this.inputText} (${this.inputText.length})`;
        }
    }
}